-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 07 月 11 日 16:20
-- 服务器版本: 5.6.12-log
-- PHP 版本: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `checkin`
--
CREATE DATABASE IF NOT EXISTS `checkin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `checkin`;

-- --------------------------------------------------------

--
-- 表的结构 `checkinlog`
--

CREATE TABLE IF NOT EXISTS `checkinlog` (
  `barcodevalue` char(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `barcodeformat` char(20) NOT NULL,
  `username` char(50) NOT NULL,
  `thismoment` datetime NOT NULL,
  `celluuid` char(50) NOT NULL,
  `latitude` char(50) NOT NULL,
  `longitude` char(50) NOT NULL,
  PRIMARY KEY (`datetime`,`username`,`celluuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 表的结构 `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `UserName` char(50) NOT NULL COMMENT '用户名，主键',
  `Password` char(50) NOT NULL COMMENT '用户密码',
  `Name` char(50) CHARACTER SET utf8 NOT NULL COMMENT '用户姓名',
  `Mobile` char(50) NOT NULL,
  `Email` char(50) NOT NULL COMMENT '用户E-mail地址',
  `actNum` char(50) DEFAULT NULL,
  `UserLevel` tinyint(4) DEFAULT NULL,
  `SignupDate` datetime DEFAULT NULL,
  `LoginTimes` tinyint(4) DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LoginIP` char(50) DEFAULT NULL,
  `LastLoginFail` char(50) DEFAULT NULL,
  `NumLoginFail` tinyint(4) DEFAULT NULL,
  `celluuid` char(50) NOT NULL,
  `latitude` char(50) NOT NULL,
  `longitude` char(50) NOT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='用户登录信息表';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
